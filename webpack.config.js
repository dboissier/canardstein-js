const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: {
    vendors: './src/js/vendors.js',
    canardstein: './src/ts/canardstein.ts'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js',
    clean: true,
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Canardstein JS',
      template: 'src/canardstein.html',
      inject: 'head'
    }),
    new CopyWebpackPlugin({
        patterns: [
          {from: './src/assets/images', to: 'images'},
          {from: './src/assets/sounds', to: 'sounds'},
          {from: './src/css', to: 'css'},
          {from: './src/assets/font', to: 'font'}
        ]
      }
    )
  ],
  module: {
    rules: [
      {
        test: /\.ts?$/,
        loader: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  devServer: {
    static: {
      directory: path.join(__dirname, 'dist')
    },
    compress: true,
    port: 9000
  }
}