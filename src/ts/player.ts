import * as BABYLON from '@babylonjs/core';
import Enemy from './enemy';

class Player {

    private readonly _scene: BABYLON.Scene;
    private readonly _gunSight: BABYLON.Mesh;
    private readonly _shotSound: BABYLON.Sound;

    private _ammoListener: (updatedValue: number) => void;
    private readonly _healthListeners: Array<(updatedValue: number) => void>;

    private readonly _damagePoints: number;
    private readonly _range: number;
    private readonly _speed: number;

    private _health: number;
    private _ammo: number;

    constructor(gunSight: BABYLON.Mesh, scene: BABYLON.Scene) {
        this._gunSight = gunSight;
        this._scene = scene;
        this._shotSound = new BABYLON.Sound("gunshot", "sounds/pistolet.wav", scene);
        this._gunSight.animations.push(this.createAnimation());
        this._healthListeners = [];

        this._damagePoints = 5;
        this._range = 12.8;
        this._speed = 0.1;

        this._ammo = 50;
        this._health = 100;
    }

    shoot(enemies: Array<Enemy>, camera: BABYLON.FreeCamera) {
        if (this._ammo == 0 || this._health <= 0) {
            return;
        }
        this._ammo--;
        this._ammoListener(this._ammo);

        this._shotSound.play();
        this._scene.beginAnimation(this._gunSight, 0, 2, false);

        const forwardRay = camera.getForwardRay(12);
        const result: BABYLON.PickingInfo = this._scene.pickSpriteWithRay(forwardRay);
        if (result.hit) {
            const foundSprite = result.pickedSprite as BABYLON.Sprite;
            const foundEnemy = enemies.find(enemy => {
                return enemy.isAlive() && enemy.getSprite() == foundSprite
            });

            if (foundEnemy !== undefined) {
                foundEnemy.takeDamage(this._damagePoints)
            }
        }
    }

    takeDamage(damage: number) {
        this._health -= damage;
        for (const listener of this._healthListeners) {
            listener(this._health);
        }
    }

    setAmmoListener(listener: (updatedValue: number) => void) {
        this._ammoListener = listener;
    }

    addHealthListener(listener: (updatedValue: number) => void) {
        this._healthListeners.push(listener);
    }

    getAmmo(): number {
        return this._ammo;
    }

    getHealth() {
        return this._health;
    }

    isDead() {
        return this._health <= 0;
    }

    private createAnimation(): BABYLON.Animation {
        const gunAnimation = new BABYLON.Animation(
            "gunAnimation",
            "material.diffuseTexture.uOffset",
            6,
            BABYLON.Animation.ANIMATIONTYPE_FLOAT,
            BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);

        const keys = [
            {
                frame: 0,
                value: 1 / 3,
                interpolation: BABYLON.AnimationKeyInterpolation.STEP
            },
            {
                frame: 1,
                value: 2 / 3,
                interpolation: BABYLON.AnimationKeyInterpolation.STEP
            },
            {
                frame: 2,
                value: 0
            }
        ];
        gunAnimation.setKeys(keys);

        return gunAnimation;
    }
}

export default Player;