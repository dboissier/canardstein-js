# CanardsteinJS

CanardsteinJS est une adptation de Canardstein, un FPS type "Wolfenstein", écrit en C# par [Sébum](https://twitter.com/akaAgar), journaliste à [CanardPC](https://www.canardpc.com). Il s'appuie sur le moteur 3D IrrlichtLime et les éléments graphiques proviennent de Didier Couly, illustrateur du magazine. Depuis le numéro 374, Sébum décrit comment coder pas à pas un FPS, tout en expliquant les algorithmes écrits par John Carmack à l'époque. 

Pour celles ou ceux qui souhaitent y jeter un coup d'oeil, une [page](http://forum.canardpc.com/threads/118984-D%C3%A9veloppez-Couch%C3%A9-saison-5-le-topic-officiel) est dédiée au projet.

J'ai vu l'opportunité d'en faire un cours que vous pouvez suivre dans la section [Wiki](https://framagit.org/dboissier/canardstein-js/wikis/home).

## Comment le construire

Prérequis :

* Node 16 et NPM 8 minimum
* Typescript 4.8.4 installé en global

A la racine du projet, lancer la commande `npm install` puis `npm run serve`.

> Note : pour celles et ceux qui travaillent sur Linux ou MacOS, je conseille de passer par [NVM](https://github.com/creationix/nvm) le gestionnaire de version de Node. Cela permet de travailler en isolation si vous avez d'autres projets Web.

## Déploiment

le jeu est déployé sur Clever Cloud et est disponible à cette [adresse](https://canardstein.coincoinpc.fr/index.html).

## Motivation

En cette année 2018, j'ai souhaité apprendre 2 technologies web :
* Le langage TypeScript qui est un sur-ensemble de Javascript permettant le typage fort des données,
* La librairie BabylonJS qui s'appuie sur la spécification WebGL, permettant donc d'effectuer de la programmation 3D dans un navigateur Web.